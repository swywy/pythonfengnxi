#皮尔逊相关系数函数
def coefficient(var1,var2):
    aavg = sum(var1)/len(var1)#变量1的平均值
    bavg = sum(var2)/len(var2)#变量2的平均值
    covab = sum([(x - aavg)*(y - bavg) for x,y in zip(var1,var2)])#变量1和变量2的协方差
    asd= math.sqrt(sum([(i - aavg)**2 for i in var1]))#变量1的标准差
    bsd= math.sqrt(sum([(j - bavg)**2 for j in var2]))#变量2的标准差
    raletive_coe = covab/(asd*bsd) #变量1和2的皮尔逊相关系数
    return round(raletive_coe,4)

#温湿度相关性分析
def relevancediagram(tems,hums,x,w,b,f):
    plt.figure(5, figsize=(16, 10))#定义图片大小和序号
    
    #定义图片标题，x轴和y轴坐标名称
    plt.title("温湿度相关性分析图")
    plt.xlabel("温度/℃")
    plt.ylabel("相对湿度/%")
    
    plt.scatter(tems, hums, color='#7FFFD4')#绘制散点图
    plt.plot(x, f)#绘制线性回归拟合曲线
    
    #标出线性回归方程和皮尔逊相关系数
    plt.text(20.1, 95.0, '线性回归方程:\nY=' + str(round(w, 3)) + 'X+' + str(round(b, 3)),
             fontdict={'size': '10', 'color': '#02082D'})
    if (coefficient(tems, hums) < 0):
        plt.text(20, 77.5, "相关系数为：" + str(coefficient(tems, hums)) + ',温湿度线性负相关',
                 fontdict={'size': '15', 'color': '#5595F9'})
    elif (coefficient(tems, hums) == 0):
        plt.text(20, 77.5, "相关系数为：" + str(coefficient(tems, hums)) + ',温湿度不存在线性相关',
                 fontdict={'size': '15', 'color': '#5595F9'})
    elif (coefficient(tems, hums) > 0):
        plt.text(20, 77.5, "相关系数为：" + str(coefficient(tems, hums)) + ',温湿度线性正相关',
                 fontdict={'size': '15', 'color': '#5595F9'})
        
    plt.savefig(savepath+'\温湿度相关性分析图.png', dpi=800)#保存图片到指定文件夹
    plt.show()
    plt.close()
