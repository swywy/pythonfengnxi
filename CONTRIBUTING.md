    import requests
    from bs4 import BeautifulSoup
    import pandas as pd
    
    def getAQI(url):
        headers={
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36'
    }
        response=requests.get(url,headers=headers)
        soup=BeautifulSoup(response.text,'lxml')
        table=soup.find('table')
        df=pd.read_html(table.prettify(),header=0)
        return df[0]
    
    
    year=2018
    cities=['beijing','shanghai']#,'shenzhen']
    for city in cities:
        dfs=[]
        filename=city+'-'+str(year)+'-AQI.csv'
        for month in range(1,3):
            url='http://www.tianqihoubao.com/aqi/'+city+'-'+str(year)+'%02d'%month+'.html'
            data=getAQI(url)
            dfs.append(data)
            #DataFrame pandas  to_sql
        for df in dfs:
            df.to_csv(filename,header=None,encoding='utf-8-sig',mode='a')
[/code]

以天气后报网站为例，抓取2018年的天气数据，其中headers设置用户代理是为了避免反爬虫，prettify()格式化读入的数据,mode='a'是设置写入csv的数据的模式，a即为append,在已有的数据后面进行添加。

2、对数据进行分析

首先读取csv文件

```code
    %matplotlib inline
    import matplotlib.pyplot as plt
    import pandas as pd
    import numpy as np
    
    data=pd.read_csv(r'beijing-2018-AQI.csv',names=['Date','Level','AQI','Rank','PM25','PM10','So2','No2','Co','O3'])
    data
[/code]

摘取某列的数据

```code
    x=data[:31].Date   # data['日期']
    y=data[:31].PM25
    plt.plot(x,y)
[/code]

对某列进行处理，python中的map会根据提供的函数对指定的序列做映射

```code
    data['month']=data.Date.map(lambda x:x[5:7])
    data.head(10)
[/code]

进行分组

```code
    aqi_mean=data.groupby('month')['AQI'].mean()
    aqi_mean
[/code]

绘图设置横坐标的值

```code
    X=np.arange(1,13)
    x_ticks=['%d月'%i for i in X]
    print(x_ticks)
[/code]

```code
    plt.rcParams['font.sans-serif']=['SimHei']
    plt.rcParams['axes.unicode_minus']=False
    y=np.array(aqi_mean)
    plt.xticks(X,x_ticks)
    plt.plot(X,y,label='AQI per month')
    plt.legend()
[/code]

上面的前两行让画出的图的中文得以显示出来，而不是显示出乱码。

计算数据

```code
    s1=data.query('month==["01","02","03"]').PM25.mean()
    s2=data.query('month==["04","05","06"]').PM25.mean()
    s3=data.query('month==["07","08","09"]').PM25.mean()
    s4=data.query('month==["10","11","12"]').PM25.mean()
    print(s1,s2,s3,s4)
[/code]

round对浮点数进行处理，第一个参数为需要处理的数据，第二个参数为保留的位数

```code
    s2=round(s2,2)
    s3=round(s3,2)
    s4=round(s4,2)
    print(s1,s2,s3,s4)
[/code]

画饼图

```code
    labels=['第%d季度'%i for i in [1,2,3,4]]
    labels
    
    pms=[s1,s2,s3,s4]
    plt.pie(pms,labels=labels,autopct='%1.2f%%',explode=[0,0,0,0.3],shadow=True)
    plt.axis('equal')
[/code]

explode是为了突出饼图的某一部分，shadow=True让饼图看上去有一些厚度


![在这里插入图片描述](https://img-blog.csdnimg.cn/20210608151750993.gif)
