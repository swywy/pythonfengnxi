//1
import pandas as pd
pd.__version__

//2
arr = [0, 1, 2, 3, 4]
df = pd.Series(arr)
df


//3

temp = {'a': 1,
        'b': 3}
data = pd.Series(temp)
print(data)

//4
#使用包含列表的字典创建DataFrame时,各个列表内元素个数必须一致
data1 = {'员工姓名':['赵一','钱明','周元'],   #默认字典的键为dataframe的字段名,不设定索引的时候,自动给出默认索引,从0到len(list)-1
       '销售业绩':[30000,20000,50000],
       '提成收入':[6000,4000,10000]}
df1 = pd.DataFrame(data1)
df1

//5

df = pd.read_csv('test.csv', encoding='gbk, sep=';')


//6

import numpy as np
import pandas as pd

data = {'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dog', 'dog'],
        'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
        'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
        'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']}

labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

df = pd.DataFrame(data, index=labels)
print(df)

                 
//7
                 
 print(df.info())
                
                 
//8               
print(df.head(3))
                 
                 
//9
                 
                 
  print(df[['animal', 'age']])

                 
//10
                 print(df.loc[df.index[[3, 4, 8]], ['animal', 'age']])

                 
 //11                
         print(df[df.age>3])
        
 //12                
         print(df[df.age.isnull()])
        
                 
 //13                
                 
                 
          print(df[(df.age >= 2) & (df.age <= 4)])
       
 //14                
                 
                 
           df.loc['f','age'] = 1.5
print(df)
      
                 
  //15               
                 
      print(df['visits'].sum())
           
                 
                 
  //16               
       print(df.groupby('animal')['age'].mean())
          
                 
  //17
#插入
df.loc['k'] = [5.5, 'dog', 'no', 2]
# 删除
df = df.drop('k')
df

// 18               
                 
                 
       print(df['animal'].value_counts())
//19

    df.sort_values(by=['age', 'visits'], ascending=[False, True])
print(df)
//20

      df['priority'] = df['priority'].map({'yes': True, 'no': False})
print(df)

//21           
    df['animal'] = df['animal'].replace('snake', 'python')
print(df)
             

//22

  df.dtypes
animal       object
age         float64
visits       object
priority      int64
dtype: object

df.age=df.age.astype(float)

df.pivot_table(index='animal', columns='visits', values='age', aggfunc='mean')

 //23

 \D：表示非数字

\w：表示一个字 [0－9a-zA-Z_]
\W：表示除[0－9a-zA-Z_]之外的字符

\s：表示一个空白字符（空格，tab，换页符等）
\S：表示一个非空白字符
这些都是正则中能够表示一类字符的原子

   /*  $以什么结尾 */
   /* 下面的正则表达式 匹配0-9的数字 里面不能是字符串 只能是数字 必须是1-5位 */
   const ad = /^[0-9]{1,5}$/.test('12345')
   console.log(ad);
   console.log(/^\d{1,5}$/.test('123899')); // 这个是简写
   /* \d 就是 代表 [0-9] */

   /* ^ 是以什么开头匹配 */
   var ps = /^h/
   console.log(ps.test('hello')); // true
   console.log(ps.test('olleh')); // false


//?=a) 零宽正向先行断言
//(?!a)零宽负向先行断言
//(?<=a)零宽正向后行断言
//(?<!a)零宽负向后行断言

//贪婪匹配：是指在整个正则表达式匹配成功的前提下，尽可能多的匹配。正则表达式默认的匹配方式是贪婪匹配，即尽可能多的匹配。

//懒惰匹配：是指在整个表达式匹配成功的前提下，尽可能少的匹配。


//密码验证
  public static boolean validatePhonePass(String pass) {
        String passRegex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,18}$";
        return !TextUtils.isEmpty(pass) && pass.matches(passRegex);
    }
    import re
def checkQQ(str):
    # 正则表达式
    pattern = r"qq:[1-9]\d{4,10}" 
    res = re.findall(pattern,str,re.I)
    return print(res)import re
———————————

//写出验证用户名的正则表达式，用户名只能输入英文、数字和下划线
function $(elementId) 

{

   return document.getElementById(elementId);

}

function checkUser()

{

	var user=$("user").value;

	var userId=$("user_prompt");

	userId.innerHTML="";

	var r1=/^[a-zA-Z0-9_]*$/;

	if(user.length<5 || user.length>16)

	{

		userId.innerHTML="请输入5-16位字符！";

		return false;

	}

	if(!r1.test(pwd))

	{

		userId.innerHTML="用户名必须包含数字、字母、下划线！";

		return false;

	}

	

	return true;

}



function checkpwd()

{

	var pwd=$("pwd").value;

	var pwdId=$("pwd_prompt");

	var reg=/^[a-zA-Z0-9_]*$/;

	pwdId.innerHTML="";

	

	if(pwd.length<6 || pwd.length>15)

	{

		pwdId.innerHTML="密码长度在6-15之间！";

		return false;

	}

	



	if(!reg.test(pwd))

	{

		pwdId.innerHTML="密码必须包含数字、字母、下划线！";

		return false;

	}

	

	return true;

	

}

//不正确，这是因为所有限定类元字符后只能紧跟?这个限定类元字符，如果
//紧跟其他限定类元字符则会报错。正确的写法是(\d{1,2})*

//使用$则表示从右边开始匹配，比如\d{2}$表示从右开始匹配两个数字

//验证用户密码强度，最少6位，至少包括1个大写字母、1个小写字母、1个数字和1个特殊字符。

^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}
—————
//24
    urllib.urlretrieve(url,"E:\Picture\%s.jpg"%0)         

//25
package day12;
 
import java.io.FileInputStream;
import java.util.List;
 
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
 
/**
 * 使用XPath检索XML数据
 * @author Administrator
 *
 */
public class XPathDemo {
	public static void main(String[] args) {
		try {
			SAXReader reader = new SAXReader();
			Document doc = reader.read(new FileInputStream("myemp.xml"));
			/*
			 * Document支持使用xpath检索数据
			 * 前提是必须引入jaxen这个jar包 
			 */
			String xpath = "/list/emp[gender='男']/age";
			List<Node> list = doc.selectNodes(xpath);
			
			for(Node ele:list) {
				System.out.println(ele.getName()+":"+ele.getText());
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}